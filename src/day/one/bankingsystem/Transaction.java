package day.one.bankingsystem;

import java.util.Date;

public class Transaction {
    private Long transactionCounter = 1L;
    private Long transactionId;
    private Date date;
    private Double amount;
    private String type;
    public Transaction(Double amount,String type){
        transactionId = transactionCounter++;
        date = new Date();
        this.amount = amount;
        this.type = type;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId=" + transactionId +
                ", date=" + date.toString() +
                ", amount=" + amount +
                ", type='" + type + '\'' +
                '}';
    }

    public Double getAmount() {
        return amount;
    }
}
