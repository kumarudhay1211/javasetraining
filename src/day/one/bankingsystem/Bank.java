package day.one.bankingsystem;

import java.util.*;

public class Bank {

    private Set<Customer> customers = new LinkedHashSet<>(); //TODO changed

    public Customer createPersonalCustomer(String name,String address,String homePhone,String workPhone){
            PersonalCustomer customer = new PersonalCustomer(name);
            customer.setAddress(address);
            customer.setHomePhone(homePhone);
            customer.setWorkPhone(workPhone);
            customers.add(customer);
            return customer;
    }

    public Customer createCommercialCustomer(String name,String address,String contactPerson,String contactPersonPhone){
        CommercialCustomer customer = new CommercialCustomer(name);
        customer.setAddress(address);
        customer.setContactPerson(contactPerson);
        customer.setContactPersonPhone(contactPersonPhone);
        customers.add(customer);
        return customer;
    }
    public Map<Customer,List<Account>> displayGrandSummary() {;
        Map<Customer,List<Account>> map = new HashMap<>();
        customers.forEach((value) -> {
            map.put(value,value.getAccounts().stream().toList());
        });
        return map;

    }
    public Map<Customer,Double> displayCustomerSummary(){
        Map<Customer,Double> map = new HashMap<>();
        customers.forEach((customer) -> {
            map.put(customer,findTotalAmount(customer.getCustomerId()));
        } );
        return map;
    }
    public Map<Customer,List<Account>> displayCustomer(Long customerId){
        Customer customer = findCustomer(customerId);
        Map<Customer, List<Account>> map = new HashMap<>();
        map.put(customer,customer.getAccounts().stream().toList());
        return map;
    }


    public Transaction makeDeposit(Long customerId, Long accountNumber, double amount) {
       Account account =  findAccount(customerId,accountNumber);
       return account.makeDeposit(amount);

    }

    public Double makeWithdraw(Long customerId, Long accountNumber, double amount){
        Account account = findAccount(customerId,accountNumber);
        return account.makeWithdraw(amount);
    }
    public Customer findCustomer(Long customerId) {
        return customers.stream()
                .filter(customer -> customer.getCustomerId()==customerId)
                .toList()
                .get(0);
    }

    public Account findAccount(Long customerId,Long accountNumber) {
        return findCustomer(customerId)
                .getAccounts()
                .stream()
                .filter(account -> account.getAccountNumber() == accountNumber)
                .toList().get(0);
    }

    public Set<Customer> getCustomers() {
        return customers;
    }
    public Double findTotalAmount(Long customerId){
        return findCustomer(customerId).getAccounts()
                .stream()
                .mapToDouble(Account::getBalance)
                .sum();
    }

}
