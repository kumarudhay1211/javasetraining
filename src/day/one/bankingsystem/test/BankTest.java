package day.one.bankingsystem.test;

import day.one.bankingsystem.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BankTest {
    private  Bank bank;
    private  Account mockAccount;
    private Long testCustomerId;
    private Long testAccountNumber;
    private double initialBalance;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        Customer customer = bank.createPersonalCustomer("John Doe", "123 Street", "123-456-7890", "098-765-4321");
        testCustomerId = customer.getCustomerId();
        testAccountNumber = customer.getAccounts().stream().toList().get(0).getAccountNumber();
        initialBalance = customer.getAccounts().stream().toList().get(0).getBalance();
    }

    @Test
    void createPersonalCustomer() {
        String name = "John Doe";
        String address = "123 Elm Street";
        String homePhone = "555-1234";
        String workPhone = "555-5678";

        Customer customer = bank.createPersonalCustomer(name, address, homePhone, workPhone);

        // Test customer creation
        assertNotNull(customer);
        assertTrue(customer instanceof PersonalCustomer);
        assertEquals(name, customer.getCustomerName());
        assertEquals(address, customer.getAddress());
        assertEquals(homePhone, ((PersonalCustomer) customer).getHomePhone());
        assertEquals(workPhone, ((PersonalCustomer) customer).getWorkPhone());

        // Test customers map update
        Set<Customer> customers = bank.getCustomers();
        assertNotNull(customers);
        assertTrue(customers.contains(customer));
        assertEquals(customer, customers.stream().filter(customer1 -> customer1.getCustomerId() == customer.getCustomerId()).toList().get(0));
    }

    @Test
    void createCommercialCustomer() {
        String name = "ABC Corporation";
        String address = "456 Main Street";
        String contactPerson = "Jane Doe";
        String contactPersonPhone = "555-7890";

        Customer customer = bank.createCommercialCustomer(name, address, contactPerson, contactPersonPhone);

        // Test customer creation
        assertNotNull(customer);
        assertTrue(customer instanceof CommercialCustomer);
        assertEquals(name, customer.getCustomerName());
        assertEquals(address, customer.getAddress());
        assertEquals(contactPerson, ((CommercialCustomer) customer).getContactPerson());
        assertEquals(contactPersonPhone, ((CommercialCustomer) customer).getContactPersonPhone());

        // Test customers map update
        Set<Customer> customers = bank.getCustomers();
        assertNotNull(customers);
        assertTrue(customers.contains(customer));
        assertEquals(customer, customers.stream().filter(i -> i.getCustomerId()==customer.getCustomerId()).toList().get(0));

    }

    @Test
    void makeDeposit() {
        double depositAmount = 1000.0;
        Transaction transaction = bank.makeDeposit(testCustomerId, testAccountNumber, depositAmount);

        assertNotNull(transaction, "Transaction should not be null");
        assertEquals(depositAmount, transaction.getAmount(), 0.01, "Deposit amount should match");
        double expectedBalance = initialBalance + depositAmount;
        Account account = bank.findAccount(testCustomerId, testAccountNumber);
        assertEquals(expectedBalance, account.getBalance(), 0.01, "Balance should be updated correctly");
    }

    @Test
    void makeWithdraw() {
        double withdrawAmount = 2000.0;
        double expectedBalanceAfterWithdrawal = initialBalance - withdrawAmount;

        // Perform withdrawal
        Double newBalance = initialBalance - bank.makeWithdraw(testCustomerId, testAccountNumber, withdrawAmount);

        assertNotNull(newBalance, "New balance should not be null");
        assertEquals(expectedBalanceAfterWithdrawal, newBalance, 0.01, "Balance should be updated correctly after withdrawal");

        // Optionally, verify the account's balance directly
        Account account = bank.findAccount(testCustomerId, testAccountNumber);
        assertEquals(expectedBalanceAfterWithdrawal, account.getBalance(), 0.01, "Account balance should match expected balance after withdrawal");
    }

    @Test
    void displayGrandSummary() {
        Map<Customer, List<Account>> grandSummaryData = bank.displayGrandSummary();

        assertNotNull(grandSummaryData);
        assertFalse(grandSummaryData.isEmpty());
    }

    @Test
    void displayCustomerSummary() {
        Map<Customer, Double> summaryData = bank.displayCustomerSummary();

        assertNotNull(summaryData);
        assertFalse(summaryData.isEmpty());
    }

    @Test
    void displayCustomer() {
        Map<Customer, List<Account>> customerData = bank.displayCustomer(testCustomerId);

        assertNotNull(customerData);
        assertFalse(customerData.isEmpty());
        assertTrue(customerData.containsKey(bank.findCustomer(testCustomerId)));
    }
}