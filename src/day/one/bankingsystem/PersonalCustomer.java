package day.one.bankingsystem;

public class PersonalCustomer extends Customer{
    private String homePhone;
    private String WorkPhone;
    public PersonalCustomer(String customerName) {
        super(customerName);
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getWorkPhone() {
        return WorkPhone;
    }

    public void setWorkPhone(String workPhone) {
        WorkPhone = workPhone;
    }

    @Override
    public String toString() {
        return "PersonalCustomer{" +
                "homePhone='" + homePhone + '\'' +
                ", WorkPhone='" + WorkPhone + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerId=" + customerId +
                '}';
    }
}
