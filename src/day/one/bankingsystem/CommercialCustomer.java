package day.one.bankingsystem;

import java.util.Arrays;

public class CommercialCustomer extends Customer{
    private String contactPerson;
    private String contactPersonPhone;

    public CommercialCustomer(String customerName) {
        super(customerName);
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactPersonPhone() {
        return contactPersonPhone;
    }

    public void setContactPersonPhone(String contactPersonPhone) {
        this.contactPersonPhone = contactPersonPhone;
    }

    @Override
    public String toString() {
        return "CommercialCustomer{" +
                "contactPerson='" + contactPerson + '\'' +
                ", contactPersonPhone='" + contactPersonPhone + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerId=" + customerId +
                '}';
    }
}
