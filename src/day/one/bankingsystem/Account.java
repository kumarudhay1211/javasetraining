package day.one.bankingsystem;

import java.util.*;

public class Account {
    private static final Double MIN_BALANCE = 10000.0;
    private static final String DEPOSIT = "deposit";
    private static final String WITHDRAW = "withdraw";
    private Long accountCounter = 1L;
    private Long accountNumber;
    private Double balance;
    private Date dateOpened;

    private Long transactionCounter = 1L;

    private Set<Transaction> transactions; //TODO changed

    public Account() {
        accountNumber = accountCounter++;
        dateOpened = new Date();
        balance = MIN_BALANCE;
        transactions = new LinkedHashSet<>();
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public Date getDateOpened() {
        return dateOpened;
    }

    public Long getTransactionCounter() {
        return transactionCounter;
    }

    public void setTransactionCounter(Long transactionCounter) {
        this.transactionCounter = transactionCounter;
    }

    public Transaction makeDeposit(Double amount){
        balance+=amount;
        Transaction transaction = new Transaction(amount,DEPOSIT);
        transactions.add(transaction);
        return transaction;
    }
    public Double makeWithdraw(Double amount){
        if(amount > balance){
            amount = 0.0;
        }else {
            balance-=amount;
            Transaction transaction = new Transaction(amount,WITHDRAW);
            transactions.add(transaction);
        }
        return amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                '}';
    }
}
