package day.one.bankingsystem;

import java.util.LinkedHashSet;
import java.util.Set;

public class Customer {

    private static Long customerCounter = 1L;
    protected String customerName;
    protected Long customerId;

    private String address;
    private Set<Account> accounts ; //TODO changed

    public Customer(String customerName){
        this.customerName = customerName;
        customerId = customerCounter++;
        Account account = new Account();
        accounts = new LinkedHashSet<>();
        accounts.add(account);
    }


    // Getters and Setters
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public static Long getCustomerCounter() {
        return customerCounter;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerName='" + customerName + '\'' +
                ", customerId=" + customerId +
                ", address='" + address + '\'' +
                ", accounts=" + accounts +
                '}';
    }
}
