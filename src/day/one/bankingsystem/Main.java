package day.one.bankingsystem;

import java.util.Scanner;

import static java.lang.System.exit;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Bank bank = new Bank();

        while (true) {
            System.out.println("\nPlease select from the menu below:");
            System.out.println("1. Create Personal Customer");
            System.out.println("2. Create Commercial Customer");
            System.out.println("3. Make Deposit");
            System.out.println("4. Make Withdrawal");
            System.out.println("5. Display Customer");
            System.out.println("6. Display Customer Summary");
            System.out.println("7. Display Grand Summary");
            System.out.println("8. Exit");
            System.out.print("Enter your choice: ");
            int ch = sc.nextInt();
            switch (ch) {
                case 1:
                    System.out.println("Enter customer name");
                    String personalCustomerName = sc.next();
                    System.out.println("Enter the address detail");
                    String personalCustomerAddress = sc.next();
                    System.out.println("Enter Home Phone Number");
                    String homePhone = sc.next();
                    System.out.println("Enter the Work Phone");
                    String workPhone = sc.next();
                    System.out.println(bank.createPersonalCustomer(personalCustomerName, personalCustomerAddress, homePhone, workPhone));
                    break;
                case 2:
                    System.out.println("Enter customer name");
                    String commercialCustomerName = sc.next();
                    System.out.println("Enter the address detail");
                    String commercialCustomerAddress = sc.next();
                    System.out.println("Enter the contact person details");
                    String contactPerson = sc.nextLine();
                    System.out.println("Enter the contact Person Phone");
                    String contactPersonPhone = sc.next();
                    System.out.println(bank.createCommercialCustomer(commercialCustomerName, commercialCustomerAddress, contactPerson, contactPersonPhone));
                    break;
                case 3:
                    System.out.println("Enter the customer Id");
                    Long customerIdDeposit = sc.nextLong();
                    System.out.println("Enter the account number");
                    Long accountNumberDeposit = sc.nextLong();
                    System.out.println("Enter the deposit amount");
                    double amountDeposit = sc.nextDouble();
                    System.out.println(bank.makeDeposit(customerIdDeposit, accountNumberDeposit, amountDeposit));
                    break;
                case 4:
                    System.out.println("Enter the customer Id");
                    Long customerIdWithdraw = sc.nextLong();
                    System.out.println("Enter the account number");
                    Long accountNumberWithdraw = sc.nextLong();
                    System.out.println("Enter the WithDraw amount");
                    double amountWithdraw = sc.nextDouble();
                    System.out.println("Transaction Details");
                    System.out.println(bank.makeWithdraw(customerIdWithdraw, accountNumberWithdraw, amountWithdraw));
                    break;
                case 5:
                    System.out.println("Enter the customer Id");
                    Long customerId = sc.nextLong();
                    System.out.println(bank.displayCustomer(customerId));
                    break;
                case 6:
                    System.out.println(bank.displayCustomerSummary());
                    break;
                case 7:
                    System.out.println(bank.displayGrandSummary());
                    break;
                case 8:
                    exit(0);
                    break;
                default:
                    System.out.println("Wrong option");
            }
        }

    }



}
